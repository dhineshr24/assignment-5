class Shop:
    def __init__(self):
        self.costs = {}

    def add_customer(self, name, cost):
        name_of_customer = name
        price_of_item = cost
        self.costs[name_of_customer] = price_of_item

    def calculate_amount(self, name):
        original_price = self.costs[name]
        if original_price <= 2000:
            discount=original_price*0.05 # discount of 5%
        elif (original_price>2000) and (original_price<=5000):
            discount = original_price * 0.25  # discount of 25%
        elif (original_price>5000) and (original_price<=10000):
             discount = original_price * 0.35  # discount of 35%
        else:
            discount = original_price * 0.5  # discount of 50%

        selling_price = original_price - discount
        print(original_price)
        return selling_price

